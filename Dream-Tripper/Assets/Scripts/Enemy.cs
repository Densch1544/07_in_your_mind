using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class Enemy : MonoBehaviour
    {
        public float flySpeed;
        public AudioSource crash;
        private bool m_FacingRight = true;

        public Vector2 pointA;
        public Vector2 pointB;
        public float time;

        private void Update()
        {
            time = Mathf.PingPong(Time.time * flySpeed, 1);
            transform.position = Vector3.Lerp(pointA, pointB, time);
            //transform.rotation = Quaternion.Lerp(0f, 180, 0, time);

            if (time > 0.99f && !m_FacingRight)
            {
                Flip();
            }
            if (time < 0.01f && m_FacingRight)
            {
                Flip();
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            crash.Play();
        }

        private void Flip()
        {
            m_FacingRight = !m_FacingRight;

            transform.Rotate(new Vector2(0f, 180f));
        }
    }
}
