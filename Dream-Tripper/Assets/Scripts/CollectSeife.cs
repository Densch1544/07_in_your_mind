﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class CollectSeife : MonoBehaviour
    {
        public AudioSource bubbles;
        public SeifeWerfen seifewerfen;
        public GameObject firePointObject;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (CompareTag("Player"))
            {
                GameEvents.current.SoapAmmunition();
                //setzt die Seifenmunition auf 10 (sowohl beim Player als auch im UI) und aktiviert das FirePoint Objekt
                //seifewerfen.munition = 10;
                firePointObject.SetActive(true);
                SeifeAnzahl.instance.ChangeScore(seifewerfen.munition);
                bubbles.Play();
                Destroy(this.gameObject);
            }
        }
    }
}