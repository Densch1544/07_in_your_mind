using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace dreamtripper
{
    public class TextEnabler : MonoBehaviour
    {
        public GameObject spechBubble;
        public CharacterCon characters;

        private void OnTriggerEnter2D(Collider2D other)
        {
            characters.highJumpEnabled = true;
            spechBubble.SetActive(true);
        }
    }
}
