using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class PlayerCon : MonoBehaviour
    {
        public CharacterCon controllers;
        public Animator animator;
        public float runSpeed = 40f;

        float horizontalMove = 0f;
        bool jump = false;
        public bool canMove = true;

        public Sprite normalSprite;
        public Sprite chargeSprite;
        public Sprite jumpSprite;
        public Sprite highJumpSprite;

        private Vector3 lastPos;

        // Update is called once per frame
        void Update()
        {
            if (!canMove)
            {
            
                return;
            }

            horizontalMove = Input.GetAxis("Horizontal") * runSpeed;
            animator.SetBool("IsWalking", horizontalMove != 0);

            if (Input.GetButtonUp("Jump"))
            {
                jump = true;
                animator.SetBool("IsCharging", false);
                animator.SetBool("IsJumping", true);
                if(controllers.m_JumpForce < 1000)
                {
                    GetComponent<SpriteRenderer>().sprite = jumpSprite;
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = highJumpSprite;
                }
            }
            
            if (Input.GetButton("Jump"))
            {
                controllers.m_JumpForce += 10;
                animator.SetBool("IsCharging", true);
                GetComponent<SpriteRenderer>().sprite = chargeSprite;

            }

            if(lastPos!=null)
            {
                animator.SetFloat("UpDown", transform.position.y - lastPos.y);
            }
         /*   else if(lastPos!=null && lastPos.y==transform.position.y)
            {
                animator.SetBool("IsJumping", false);
            }
            */
            lastPos = transform.position;

        }

        public void OnLanding()
        {
            animator.SetBool("IsJumping", false);
            animator.SetBool("IsCharging", false);
            //GetComponent<SpriteRenderer>().sprite = normalSprite;

        }
        void FixedUpdate()
        {
            if (!canMove)
            {
                return;
            }
            // Move our character
            controllers.Move(horizontalMove * Time.fixedDeltaTime, jump);
            
            jump = false;
        }
    }
}
