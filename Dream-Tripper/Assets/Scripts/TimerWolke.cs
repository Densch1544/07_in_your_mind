using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{
    public class TimerWolke : MonoBehaviour
    {
        public Color SpriteColor;
        public bool onCloud = false;

        public Collider2D plattform;
        private void Start()
        {
            //SpriteColor.a = 1f;
            //plattform = GetComponentInChildren<BoxCollider2D>();

        }
        void Update()
        {
            if (onCloud == true)
            {
                SpriteColor.a -= 0.5f * Time.deltaTime;
                if(SpriteColor.a < 0f)
                {
                    plattform.enabled = false;
                }
            }
            else
            {
                SpriteColor.a += 0.4f * Time.deltaTime;
            }
            GetComponent<SpriteRenderer>().color = SpriteColor;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            onCloud = true;
        }
        void OnTriggerExit2D(Collider2D other)
        {
            onCloud = false;
            plattform.enabled = true;
        }
    }
}
