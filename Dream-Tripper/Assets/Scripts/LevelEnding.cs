using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace dreamtripper
{
    public class LevelEnding : MonoBehaviour
    {

        private void OnTriggerEnter2D(Collider2D other)
        {
                SceneManager.LoadScene("Outro");          
        }
    }
}
