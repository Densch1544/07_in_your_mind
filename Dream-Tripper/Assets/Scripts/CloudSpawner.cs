using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace dreamtripper
{

    public class CloudSpawner : MonoBehaviour
    {
        private float timer = 0;
        public GameObject[] prefab;
        public GameObject clouds;

        void Update()
        {
            timer += Time.deltaTime;

            if (timer >= 5)
            {
                GameObject cloud = GameObject.Instantiate(prefab[Random.Range(0, prefab.Length)], this.transform.position, Quaternion.identity, clouds.transform);
                cloud.GetComponent<Clouds>().startposition = this.transform.position;


                timer = 0;

            }
        }
    }

}
